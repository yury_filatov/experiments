package com.test.roomtest;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface TableDao {
    @Insert
    void add(Table table);

    @Query("SELECT * FROM 'table'")
    Flowable<List<Table>> getAll();

    @Delete
    void remove (List<Table> tables);

}

