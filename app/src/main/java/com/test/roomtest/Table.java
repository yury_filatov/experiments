package com.test.roomtest;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Table {
    @PrimaryKey
    @NonNull
    public String id;

    public String value;
}
