package com.test.roomtest;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.support.v4.app.NotificationCompat.PRIORITY_MIN;

public class CleanService extends android.app.Service {

    private Db db = null;
    private Disposable listener = null;

    private final int ID_SERVICE = 101;
    private final String CHANEL_ID = "clipboard_channel";

    private NotificationCompat.Builder notificationBuilder;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Get db
        db = DbSingleton.get();

        // Create the Foreground Service
        notificationBuilder = new NotificationCompat.Builder(this, CHANEL_ID);


        Notification notification = createNotification(false);

        startForeground(ID_SERVICE, notification);
    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Subscribe on table changes
        listener = db.tableDao()
                .getAll()
                .observeOn(Schedulers.io())
                .subscribe(
                        (list) -> {
                            if (list != null && list.size()>0)
                                // remover these records from db
                                db.tableDao().remove(list);
                        },
                        (error) -> {
                            error = error;
                        },
                        () -> {}
                );

        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        if (listener != null)
        {
            if (!listener.isDisposed())
                listener.dispose();

            listener = null;
        }
        super.onDestroy();
    }


    private Notification createNotification(boolean isConnected) {
        return notificationBuilder.setOngoing(true)
                .setSmallIcon(isConnected ? R.drawable.ic_launcher_background: R.drawable.ic_launcher_background)
                .setContentTitle("Service")
                .setPriority(PRIORITY_MIN)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .build();
    }


}
