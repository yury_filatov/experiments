package com.test.roomtest;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Table.class}, version = 1)
public abstract class Db extends RoomDatabase {
    public abstract TableDao tableDao();

}


