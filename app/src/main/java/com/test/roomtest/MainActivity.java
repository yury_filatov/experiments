package com.test.roomtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    Db db = null;
    Disposable listener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        subscribe();
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribe();
    }

    @Override
    protected void onStop() {
        if (listener!= null) {
            if (!listener.isDisposed())
                listener.dispose();
            listener = null;
        }
        super.onStop();
    }

    private void subscribe() {
        // Get db
        db = DbSingleton.get();


        listener = db.tableDao()
                .getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        (list) -> {
                            TextView counts = findViewById(R.id.recNum);
                            counts.setText("Records: "+list.size());
                        }
                );


        Button addRecord = findViewById(R.id.addRecord);
        addRecord.setOnClickListener(v -> {
            Observable.fromCallable(()->{
                Table t = new Table();
                t.id = UUID.randomUUID().toString();
                db.tableDao().add(t);
                return true;
            }).subscribeOn(Schedulers.io()).subscribe();

        });

        Button addRecords = findViewById(R.id.addRecords);
        addRecords.setOnClickListener(v -> {
            Observable.fromCallable(()->{
                for (int i=0; i<100;i++) {
                    Table t = new Table();
                    t.id = UUID.randomUUID().toString();
                    db.tableDao().add(t);
                }
                return true;
            }).subscribeOn(Schedulers.io()).subscribe();

        });
    }
}
