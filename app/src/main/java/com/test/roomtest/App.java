package com.test.roomtest;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class App extends Application {

    private Db db = null;
    private Context context = null;

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        //DB singletone
        DbSingleton.create(context);
        db = DbSingleton.get();

        // Start service
        Intent intent = new Intent(context,CleanService.class);
        ContextCompat.startForegroundService(context, intent);

        Toast.makeText(this, "App started", Toast.LENGTH_LONG).show();

    }
}
