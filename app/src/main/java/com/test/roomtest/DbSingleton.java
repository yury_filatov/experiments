package com.test.roomtest;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DbSingleton {

    private static Db db = null;

    public static void create(Context context) {
        if (db != null)
            return;
        db = Room.databaseBuilder(context, Db.class, "db").build();
    }

    public static Db get() {
        return db;
    }

}
